import random

def Welcome():
    print ("\n==========================")
    print ("Bienvenido\n\nEste es un Juego sencillo\nde Piedra, Papel o Tijera")
    print ("-------------------------")
    print ("Jugará contra la CPU\n\nSuerte")
    print ("==========================\n")
    return

def SeleccionUser():
    while True:
        Op_User = input("\nPiedra  (z)\nPapel   (x)\nTijera  (c)\n\nTerminar Juego  (s)\n\nElija su opcion : ")
        if Op_User == "z":      
            Op_User = "Piedra"
            break
        elif Op_User == "x":    
            Op_User = "Papel"
            break
        elif Op_User == "c":    
            Op_User = "Tijera"
            break
        elif Op_User == "s":
            break
        else:                   
            print ("Seleccione una opcion válida")
    return Op_User

def Juego():
    Opciones = ["Piedra","Papel","Tijera"]
    Contador = 1
    Win = 0
    Lose = 0
    Draw = 0
    while True:
        print ("\n==========================")
        print ("Partida N°",Contador)
        print ("==========================")
        Op_CPU = random.choice(Opciones)
        Op_User = SeleccionUser()
        print ("-------------------------")

        if (Op_User == "Tijera" and Op_CPU == "Papel") or (Op_User == "Papel" and Op_CPU == "Piedra") or (Op_User == "Piedra" and Op_CPU == "Tijera"):
            print ("\nGanaste!!!")
            print ("Elegiste",Op_User,"y la CPU eligio",Op_CPU)
            Win += 1
        elif (Op_User == "Tijera" and Op_CPU == "Piedra") or (Op_User == "Papel" and Op_CPU == "Tijera") or (Op_User == "Piedra" and Op_CPU == "Papel"):
            print ("\nPerdiste!")
            print ("Elegiste",Op_User,"y la CPU eligio",Op_CPU)
            Lose += 1

        elif Op_User == "s":
            print ("\n\n\nFinalizando e imprimiendo estadísticas\n\n\n")
            break

        else:
            print ("\nEmpate!")
            print ("Elegiste",Op_User,"y la CPU eligio",Op_CPU)
            Draw += 1
        print ("\n-------------------------")
        Contador += 1
    return [Win,Lose,Draw]

def Despedida(Fin):
    print ("\n========================")
    print ("Fin del Juego\nHa ganado   ",Fin[0],"partidas","\nHa perdido  ",Fin[1],"partidas","\nHa empatado ",Fin[2],"partidas")
    print ("-----------------------")
    print ("\nHasta la próxima\n")
    print ("========================\n")
    while True:
        Exit = input("")
        if Exit != 55:
            break
    return


Welcome()
Fin = Juego()
Despedida(Fin)
